# e3-Susannes_Test_IOC

This is a test IOC which generates random and sine data. Those generated data will be arranged to carry over PVs (see below).
This IOC depends on 2 e3 modules, they are e3-sequencer and e3-sine-random.
It also employ e3-recsync module to communicate with channel finder.

## Dependent modules:

  - e3-sequencer
  - e3-sine-random (This is an unofficial, testing only module)

## Using modules:

  - e3-recsync

## To run the IOC for testing (not officially deployed with deploy tool):

  - Make sure that a minimum e3 environment has been installed if there is no e3 avaiable (including EPICS base, require, e3-sequencer, e3-sine-random and e3-recsync).
  - Download the IOC:

```sh
      $ git clone  <this_repository_URL>
      $ cd e3-Sunannes_Test_IOC
```

  - Edit the last line of the IOC start up script "st_ioc" to point it to the right "iocsh".
  - Add the host machine's IP address to the "EPICS_CA_ADDR_LIST".
  - Start the IOC using the IOC start up script: 

```sh
      $ ./st_ioc
```
  - Or if e3 environment has been setup, one can do:
```sh
      $ iocsh -r sine_random  ./cmds/st.cmd
```
  - Or if there is a cell installed sine_random module, one can do:
```sh
      $ iocsh -l /path/to/cell/installation -r sine_random  ./cmds/st.cmd
```

## PVs and data that they carry:

```sh
     Susannes_test_IOC:Sine_Random:sineWaveformOut   :   Contains a complete cycle of sine data(total 360 elements), calculated from angle-degree 1 to 360.
     Susannes_test_IOC:Sine_Random:sineOut           :   Sine data calculated from angle-degree 1 to 360 cyclically, step = 1.
     Susannes_test_IOC:Sine_Random:sineValue         :   Same as above "sineOut".
     Susannes_test_IOC:Sine_Random:aRandomNum        :   Integer random number from 0 to 9.
     Susannes_test_IOC:Sine_Random:randomNumber_01   :   Random number from 0 to 1.
```

 ## Some other PVs for testing only.
