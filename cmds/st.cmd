
require recsync

epicsEnvSet("IOCNAME", "Sine_Random")
epicsEnvSet("P", "Susannes_test_IOC")
epicsEnvSet("user", "$(P):$(IOCNAME)")

dbLoadRecords("randomsin.db", "user=$(user)")

afterInit("seq randomSin", "user=$(user)")
afterInit("echo")
afterInit("seqShow")
afterInit("echo")

iocInit()

#dbl > PVs.list
date

